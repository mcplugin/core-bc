package cn.mcp.core.bc

import net.md_5.bungee.api.plugin.Plugin
import net.md_5.bungee.config.Configuration
import net.md_5.bungee.config.YamlConfiguration
import java.io.*
import java.util.logging.Level

open class BCPlugin : Plugin() {

    private val yamlConfigProvider = YamlConfiguration.getProvider(YamlConfiguration::class.java)
    private val configFile = File(dataFolder, "config.yml")
    private var newConfig: Configuration? = null

    fun getConfig(): Configuration? {
        if (newConfig == null) {
            reloadConfig()
        }
        return newConfig
    }

    fun saveConfig() {
        try {
            yamlConfigProvider.save(getConfig(), configFile)
        } catch (ex: IOException) {
            logger.log(Level.SEVERE, "Could not save config to $configFile", ex)
        }
    }

    fun reloadConfig() {
        newConfig = yamlConfigProvider.load(configFile)
//        val defConfigStream: InputStream = getResourceAsStream("config.yml") ?: return
//        newConfig = yamlConfigProvider.load(defConfigStream)
    }

    fun saveDefaultConfig() {
        if (!configFile.exists()) {
            saveResource("config.yml", false)
        }
    }

    fun saveResource(resourcePath: String, replace: Boolean) {
        val `in`: InputStream = getResourceAsStream(resourcePath)
            ?: throw IllegalArgumentException("The embedded resource '$resourcePath' cannot be found in $file")
        val outFile = File(dataFolder, resourcePath)
        val lastIndex = resourcePath.lastIndexOf('/')
        val outDir = File(dataFolder, resourcePath.substring(0, if (lastIndex >= 0) lastIndex else 0))
        if (!outDir.exists()) {
            outDir.mkdirs()
        }
        try {
            if (!outFile.exists() || replace) {
                val out: OutputStream = FileOutputStream(outFile)
                val buf = ByteArray(1024)
                var len: Int
                while (`in`.read(buf).also { len = it } > 0) {
                    out.write(buf, 0, len)
                }
                out.close()
                `in`.close()
            } else {
                logger.log(
                    Level.WARNING,
                    "Could not save " + outFile.name + " to " + outFile + " because " + outFile.name + " already exists."
                )
            }
        } catch (ex: IOException) {
            logger.log(Level.SEVERE, "Could not save " + outFile.name + " to " + outFile, ex)
        }
    }
}